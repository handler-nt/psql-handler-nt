const promise = require('bluebird');
promise.config({longStackTraces: true});
const pgp = require('pg-promise')({promiseLib: promise});

let db = null;


function connect(config)
{
  if (!config)
    return;

  db = pgp(
  {
    host: config.host,
    port: config.port,
    database: config.database,
    user: config.user,
    password: config.password
  });
}


module.exports =
{
  connect,
  connection : () => {return db;}
};

