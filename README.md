# PSQL Handler
This module allows a simple connection to a postgres database.<br>
It uses the <b>pg-promise</b> module.

## Installation
<code>$ yarn install psql-handler-nt --save </code>

## Documentation
- <code>connect(config)</code><br>
	This function connects to the postgres 
	@param config {object}
	
	Example of config
	````json
	{ 
	    "host": "127.0.0.1",
     "port": 5432,
     "database": "test",
     "user": "user",
     "password": "SECRET"
  }
	````

- <code>connection()</code><br>
	Returns the current connection
 
	